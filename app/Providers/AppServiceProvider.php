<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\HairSalon;
use App\Observers\HairSalonObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        HairSalon::observe(HairSalonObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
