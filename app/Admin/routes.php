<?php

use Illuminate\Routing\Router;

Admin::registerHelpersRoutes([
    'middleware' => ['web', 'admin.permission:allow,administrator']
]);

Route::group([
    'prefix'        => config('admin.prefix'),
    'namespace'     => Admin::controllerNamespace(),
    'middleware'    => ['web', 'admin', 'prevent-export'],
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('hair-salon', 'HairSalonAdminController', ['only' => ['index', 'show', 'update', 'edit']]);
    $router->resource('hair-salon-image', 'HairSalonImageAdminController', ['only' => ['index', 'show', 'update', 'edit']]);
    $router->resource('user', 'UserAdminController', ['only' => ['index', 'show']]);

});
