<?php

namespace App\Admin\Controllers;

use App\Models\HairSalonImage;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class HairSalonImageAdminController extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Salon Image');
            $content->description('Salon Image');

            $content->body($this->grid());
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit Hair Salon');
            $content->description('Hair salon information');

            $content->body($this->form()->edit($id));
        });
    }
    
    protected function grid()
    {
        return Admin::grid(HairSalonImage::class, function (Grid $grid) {
            $grid->model()->orderBy('style_hair_id', 'desc');
            $grid->hair_salon()->display(function ($salon) {
                $hair_salon_id = $salon['hair_salon_id'];
                $name = $salon['name'];
                $href = action('\App\Admin\Controllers\HairSalonAdminController@index', ['hair_salon_id' => $hair_salon_id]);

                return "<a href=$href>$name</a>";
            });

            $grid->thumbnail(__('thumbnail'))->display(function ($image) {
                $src = config('admin.upload.host') . $image;
                $not_found_msg = __('photo') . ' ' . __('not found');
                return "<img src='$src' alt='$not_found_msg'
                        style='max-width:200px;max-height:200px' class='img img-thumbnail' />";
            });

            $grid->filter(function ($filter) {
                $filter->equal('is_delete', __('deleted'));
                $filter->equal('is_publish', __('published'));
                 $filter->is('hair_salon_id', strtoupper(__('id')));
                 $filter->between('create_date', __('created within'))->datetime();
                 $filter->disableIdFilter();
            });

            $grid->disableExport();
            // $grid->disableActions();
            $grid->disableCreation();
            $grid->tools(function ($tools) {
                $tools->batch(function ($batch) {
                    $batch->disableDelete();
                });
            });
        });
    }
    
    protected function form()
    {
        return Admin::form(HairSalonImage::class, function (Form $form) {

            $form->display('style_hair_id', strtoupper(__('id')));
            $form->image('image', __('image'))->readOnly();
            // $form->display('name', __('name'));
            // $form->switch('status', __('status'));
            $form->display('create_date', __('create date'));
        });
    }
}
