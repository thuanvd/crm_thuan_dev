<?php

namespace App\Admin\Controllers;

use App\Models\HairSalon;
use App\Models\User;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class HairSalonAdminController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Hair Salon');
            $content->description('Hair salon information');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(HairSalon::class, function (Grid $grid) {

            $grid->model()->orderBy('hair_salon_id', 'desc');

            $grid->hair_salon_id(strtoupper(__('id')))->sortable();
            $grid->thumbnail(__('thumbnail'))->image('', 50, 50);
            $grid->name(__('name'));
            $grid->user(__('owner'))->display(function ($user) {
                $user_id = $user['user_id'];
                $firstname = $user['firstname'];
                $href = action('\App\Admin\Controllers\UserAdminController@index', ['user_id' => $user_id]);

                return "<a href=$href>$firstname</a>";
            });
            $grid->status(__('status'))->switch();
            $grid->create_date(__('create date'));
            $grid->images(__('photos'))->display(function ($images) {
                $published = array();
                foreach ($images as $img) {
                    if ($img['is_publish'] && !$img['is_delete']) {
                        array_push($published, $img);
                    }
                }

                $href = action('\App\Admin\Controllers\HairSalonImageAdminController@index', [
                    'hair_salon_id' => $this->hair_salon_id,
                    'is_delete' => 0,
                    'is_publish' => 1
                ]);

                return "<a href='$href'>" . sizeof($published) . " " . __('photos') . "</a>";
            });

            $grid->filter(function ($filter) {
                $filter->like('name', __('name'));
                $filter->between('create_date', __('created within'))->datetime();
            });

            $grid->disableExport();
            $grid->disableActions();
            $grid->disableCreation();
            $grid->tools(function ($tools) {
                $tools->batch(function ($batch) {
                    $batch->disableDelete();
                });
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(HairSalon::class, function (Form $form) {

            $form->display('hair_salon_id', strtoupper(__('id')));
            $form->image('image', __('image'))->readOnly();
            $form->display('name', __('name'));
            $form->switch('status', __('status'));
            $form->display('create_date', __('create date'));
        });
    }
}
