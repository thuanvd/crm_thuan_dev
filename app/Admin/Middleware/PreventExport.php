<?php

namespace App\Admin\Middleware;

use Closure;

class PreventExport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $input = $request->input();
        unset($input['_export_']);
        $request->replace($input);

        return $next($request);
    }
}
