<?php

namespace App\Observers;

use App\Models\HairSalon;
use GuzzleHttp\Client as HttpClient;

class HairSalonObserver
{
    public function updated(HairSalon $salon)
    {
        if ($salon->haveStatusUpdate() && $salon->status == 1)
        {
            $client = new HttpClient();
            $client->request('GET', config('app.api_host') . 'salon/approved/' . $salon->hair_salon_id);
        }
    }
}