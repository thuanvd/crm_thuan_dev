<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HairSalonImage extends Model
{
    protected $connection = 'app_db';
    protected $table = 'style_hairs';
    protected $primaryKey = 'style_hair_id';
    
    public $timestamps = false;

    public function hair_salon()
    {
        return $this->belongsTo(HairSalon::class, 'hair_salon_id');
    }
}
