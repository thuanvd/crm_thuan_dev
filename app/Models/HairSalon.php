<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HairSalon extends Model
{
    protected $connection = 'app_db';
    protected $table = 'hair_salon';
    protected $primaryKey = 'hair_salon_id';
    protected $haveStatusUpdate = false;

    public $timestamps = false;

    public function setAttribute($column, $value)
    {
        if ($column != 'status')
        {
            abort(403);
        }
        $this->haveStatusUpdate = true;

        parent::setAttribute($column, $value);
    }

    public function haveStatusUpdate()
    {
        return $this->haveStatusUpdate;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function images()
    {
        return $this->hasMany(HairSalonImage::class, 'hair_salon_id');
    }
}
