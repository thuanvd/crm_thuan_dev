-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.28-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for myhabfit_api
CREATE DATABASE IF NOT EXISTS `myhabfit_api` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `myhabfit_api`;

-- Dumping structure for table myhabfit_api.advertisement
CREATE TABLE IF NOT EXISTS `advertisement` (
  `ad_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name_ad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobule_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.album_photo
CREATE TABLE IF NOT EXISTS `album_photo` (
  `album_photo_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`album_photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.coupon
CREATE TABLE IF NOT EXISTS `coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_user_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_type` tinyint(4) NOT NULL,
  `coupon_type` tinyint(4) NOT NULL,
  `value` double NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `minimum_booking_amount` double DEFAULT NULL,
  `applies_to_user` int(11) DEFAULT NULL,
  `applies_to_shop` int(11) DEFAULT NULL,
  `usage_limit` int(11) DEFAULT NULL,
  `usage_per_user` int(11) DEFAULT NULL,
  `times_used` int(11) DEFAULT '0',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.customer_support
CREATE TABLE IF NOT EXISTS `customer_support` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `is_sent` tinyint(1) DEFAULT '0',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.haircare_brands
CREATE TABLE IF NOT EXISTS `haircare_brands` (
  `haicare_brand_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_vi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`haicare_brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.haircare_brands_other
CREATE TABLE IF NOT EXISTS `haircare_brands_other` (
  `haicare_brand_other_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `is_checked` tinyint(1) NOT NULL DEFAULT '1',
  `name_vi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`haicare_brand_other_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.hair_salon
CREATE TABLE IF NOT EXISTS `hair_salon` (
  `hair_salon_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hair_salon_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `rule_at_business_id` int(11) DEFAULT NULL,
  `total_booking` int(11) DEFAULT '0',
  `num_of_rate` int(11) DEFAULT '0',
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` mediumtext COLLATE utf8_unicode_ci,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year_of_experience` float DEFAULT NULL,
  `code2` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `num_of_booking` int(11) DEFAULT '0',
  `num_of_balance` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `end_trial_date` date DEFAULT NULL,
  `salon_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`hair_salon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.hair_salon_image
CREATE TABLE IF NOT EXISTS `hair_salon_image` (
  `hair_salon_image_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hair_salon_id` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`hair_salon_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.hair_salon_photo
CREATE TABLE IF NOT EXISTS `hair_salon_photo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hair_salon_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.hair_salon_type
CREATE TABLE IF NOT EXISTS `hair_salon_type` (
  `hair_salon_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`hair_salon_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.hair_service
CREATE TABLE IF NOT EXISTS `hair_service` (
  `hair_service_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `short_price` double DEFAULT NULL,
  `short_checked` tinyint(1) DEFAULT '0',
  `medium_price` double DEFAULT NULL,
  `medium_checked` tinyint(1) DEFAULT '0',
  `long_price` double DEFAULT NULL,
  `long_checked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`hair_service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.hair_service_category
CREATE TABLE IF NOT EXISTS `hair_service_category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_order` int(11) DEFAULT '0',
  `name_vi` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.hair_service_manager
CREATE TABLE IF NOT EXISTS `hair_service_manager` (
  `service_manager_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `short_price` double DEFAULT NULL,
  `short_checked` tinyint(1) DEFAULT '0',
  `medium_price` double DEFAULT NULL,
  `medium_checked` tinyint(1) DEFAULT '0',
  `long_price` double DEFAULT NULL,
  `long_checked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`service_manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.length_of_hair
CREATE TABLE IF NOT EXISTS `length_of_hair` (
  `length_of_hair_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_vi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`length_of_hair_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.package
CREATE TABLE IF NOT EXISTS `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `num_of_style` int(11) NOT NULL,
  `is_delete` tinyint(1) DEFAULT '0',
  `create_date` datetime NOT NULL,
  `currency` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.questions
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.rate_of_salon
CREATE TABLE IF NOT EXISTS `rate_of_salon` (
  `rate_of_salon` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hair_salon_id` int(11) DEFAULT NULL,
  `number_of_rate` tinyint(1) DEFAULT NULL,
  `noted` mediumtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`rate_of_salon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.rule_at_bussiness
CREATE TABLE IF NOT EXISTS `rule_at_bussiness` (
  `rule_at_business_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`rule_at_business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.setting_email_notification
CREATE TABLE IF NOT EXISTS `setting_email_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `type` tinyint(2) DEFAULT '0',
  `is_notification` tinyint(1) DEFAULT '0',
  `is_notification_booking` tinyint(1) DEFAULT '0',
  `is_notification_user_chat` tinyint(1) DEFAULT '0',
  `is_notification_user_review` tinyint(1) DEFAULT '0',
  `is_notification_prof_chat` tinyint(1) DEFAULT '0',
  `is_notification_announced` tinyint(1) DEFAULT '0',
  `is_professional` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.setting_push_notification
CREATE TABLE IF NOT EXISTS `setting_push_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token_device` varchar(255) DEFAULT NULL,
  `type` tinyint(2) DEFAULT '2',
  `is_notification` tinyint(1) DEFAULT '0',
  `is_notification_booking` tinyint(1) DEFAULT '0',
  `is_notification_user_chat` tinyint(1) DEFAULT '0',
  `is_notification_user_review` tinyint(1) DEFAULT '0',
  `is_notification_prof_chat` tinyint(1) DEFAULT '0',
  `is_notification_announced` tinyint(1) DEFAULT '0',
  `is_professional` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.setting_sms_notification
CREATE TABLE IF NOT EXISTS `setting_sms_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `type` tinyint(2) DEFAULT '1',
  `is_notification` tinyint(1) DEFAULT '0',
  `is_notification_booking` tinyint(1) DEFAULT '0',
  `is_notification_user_chat` tinyint(1) DEFAULT '0',
  `is_notification_user_review` tinyint(1) DEFAULT '0',
  `is_notification_prof_chat` tinyint(1) DEFAULT '0',
  `is_notification_announced` tinyint(1) DEFAULT '0',
  `is_professional` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs
CREATE TABLE IF NOT EXISTS `style_hairs` (
  `style_hair_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hair_salon_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `number_like` int(11) DEFAULT '0',
  `number_booking` int(11) NOT NULL DEFAULT '0',
  `number_view` int(11) NOT NULL DEFAULT '0',
  `noted` text COLLATE utf8_unicode_ci,
  `create_date` datetime NOT NULL,
  `album_photo_id` int(11) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `type_of_hair_id` int(11) DEFAULT NULL,
  `length_of_hair_id` int(11) DEFAULT NULL,
  `length_of_service` int(11) DEFAULT NULL,
  `promotion_date` datetime DEFAULT NULL,
  `term_of_condition` text COLLATE utf8_unicode_ci,
  `price_promotion` double DEFAULT NULL,
  `promotion_start_date` datetime NOT NULL,
  `promotion_end_date` datetime NOT NULL,
  `users_manager_promotion_id` int(11) NOT NULL,
  `is_publish` tinyint(1) NOT NULL DEFAULT '0',
  `is_promotion` tinyint(1) NOT NULL DEFAULT '0',
  `service_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `current_price` double NOT NULL,
  `old_price` double NOT NULL,
  PRIMARY KEY (`style_hair_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_color
CREATE TABLE IF NOT EXISTS `style_hairs_color` (
  `style_hair_color_id` int(11) NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `hair_color_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`style_hair_color_id`),
  KEY `style_hair_id` (`style_hair_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_haircare_brands
CREATE TABLE IF NOT EXISTS `style_hairs_haircare_brands` (
  `style_haircare_brand_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `haicare_brand_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`style_haircare_brand_id`),
  KEY `style_hair_id` (`style_hair_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_haircare_brands_other
CREATE TABLE IF NOT EXISTS `style_hairs_haircare_brands_other` (
  `style_haircare_brand_other_id` int(11) NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `haicare_brand_other_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`style_haircare_brand_other_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_inspriring_hair_style
CREATE TABLE IF NOT EXISTS `style_hairs_inspriring_hair_style` (
  `style_inspriring_hair_style_id` int(11) NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `inspriring_hair_style_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`style_inspriring_hair_style_id`),
  KEY `style_hair_id` (`style_hair_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_other
CREATE TABLE IF NOT EXISTS `style_hairs_other` (
  `style_other_id` int(11) NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `other_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`style_other_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_price_normal
CREATE TABLE IF NOT EXISTS `style_hairs_price_normal` (
  `style_hairs_price_normal_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_manager_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `style_hair_id` int(11) DEFAULT NULL,
  `short_price` double DEFAULT NULL,
  `short_checked` tinyint(1) DEFAULT '0',
  `medium_price` double DEFAULT NULL,
  `medium_checked` tinyint(1) DEFAULT '0',
  `long_price` double DEFAULT NULL,
  `long_checked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`style_hairs_price_normal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_price_promotion
CREATE TABLE IF NOT EXISTS `style_hairs_price_promotion` (
  `style_hairs_price_promotion_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `manager_promotion_id` int(11) DEFAULT NULL,
  `service_manager_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `style_hair_id` int(11) DEFAULT NULL,
  `short_price` double DEFAULT NULL,
  `short_price_promotion` double DEFAULT NULL,
  `short_promotion_checked` tinyint(1) DEFAULT '0',
  `medium_price` double DEFAULT NULL,
  `medium_price_promotion` double DEFAULT NULL,
  `medium_promotion_checked` tinyint(1) DEFAULT '0',
  `long_price` double DEFAULT NULL,
  `long_price_promotion` double DEFAULT NULL,
  `long_promotion_checked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`style_hairs_price_promotion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_professional_hair_service
CREATE TABLE IF NOT EXISTS `style_hairs_professional_hair_service` (
  `style_professional_hair_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `professional_hair_service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`style_professional_hair_service_id`),
  KEY `style_hair_id` (`style_hair_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hairs_type_of_hair
CREATE TABLE IF NOT EXISTS `style_hairs_type_of_hair` (
  `style_type_of_hair_id` int(11) NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `type_of_hair_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`style_type_of_hair_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hair_color
CREATE TABLE IF NOT EXISTS `style_hair_color` (
  `hair_color_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_vi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`hair_color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hair_comment
CREATE TABLE IF NOT EXISTS `style_hair_comment` (
  `style_hair_comment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `num_of_rate` tinyint(1) DEFAULT NULL,
  `comment` mediumtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`style_hair_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_hair_cut
CREATE TABLE IF NOT EXISTS `style_hair_cut` (
  `hair_cut_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`hair_cut_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_inspriring_hair_style
CREATE TABLE IF NOT EXISTS `style_inspriring_hair_style` (
  `inspriring_hair_style_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_vi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`inspriring_hair_style_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_other
CREATE TABLE IF NOT EXISTS `style_other` (
  `other_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_vi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`other_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_professional_hair_service
CREATE TABLE IF NOT EXISTS `style_professional_hair_service` (
  `professional_hair_service_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_vi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`professional_hair_service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.style_professional_hair_services
CREATE TABLE IF NOT EXISTS `style_professional_hair_services` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.type_of_hair
CREATE TABLE IF NOT EXISTS `type_of_hair` (
  `type_of_hair_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_vi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`type_of_hair_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year_of_experience` int(11) DEFAULT NULL,
  `about` text COLLATE utf8_unicode_ci,
  `birthday` date DEFAULT NULL,
  `active` smallint(1) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'normal',
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` int(11) DEFAULT NULL,
  `latitude` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `is_professional` int(1) NOT NULL DEFAULT '0',
  `imei` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `job_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `award_description` text COLLATE utf8_unicode_ci,
  `personal_account_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qb_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `time_reminder` int(11) NOT NULL DEFAULT '0',
  `salon_id` int(11) DEFAULT NULL,
  `invite_status` tinyint(1) DEFAULT '0',
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AVG_ROW_LENGTH=10;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_active_email
CREATE TABLE IF NOT EXISTS `users_active_email` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_send` tinyint(1) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `is_professional` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_availability_time
CREATE TABLE IF NOT EXISTS `users_availability_time` (
  `user_time_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `is_availability` int(1) DEFAULT '0',
  `start_break` time DEFAULT NULL,
  `end_break` time DEFAULT NULL,
  `is_break_availability` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`user_time_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_awards
CREATE TABLE IF NOT EXISTS `users_awards` (
  `user_award_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `award_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_award_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_bank_account
CREATE TABLE IF NOT EXISTS `users_bank_account` (
  `user_bank_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routing_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verify_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_business` tinyint(1) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_bank_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_bookings
CREATE TABLE IF NOT EXISTS `users_bookings` (
  `user_booking_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_hair_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_prof` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_prof_id` int(11) NOT NULL,
  `hair_salon_id` int(11) NOT NULL,
  `fullname_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_prof` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salon_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `length_of_service` int(11) NOT NULL,
  `length_of_hair_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `promo_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `discount_price` double DEFAULT '0',
  `final_price` double DEFAULT NULL,
  `title_booking` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noted` mediumtext COLLATE utf8_unicode_ci,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `status_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `is_count_promo_code` tinyint(4) NOT NULL DEFAULT '0',
  `is_prof_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_user_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `booking_day` int(11) DEFAULT NULL,
  `booking_week` int(11) DEFAULT NULL,
  `booking_month` int(11) DEFAULT NULL,
  `booking_quarter` int(11) DEFAULT NULL,
  `booking_year` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_bookings_change_status_log
CREATE TABLE IF NOT EXISTS `users_bookings_change_status_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_booking_id` int(11) DEFAULT NULL,
  `user_id_change` int(11) DEFAULT NULL,
  `is_professional` tinyint(1) DEFAULT NULL,
  `pre_status` tinyint(3) DEFAULT NULL,
  `status` tinyint(3) DEFAULT NULL,
  `noted` text,
  `pre_date` datetime NOT NULL,
  `new_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_bookings_promo_code
CREATE TABLE IF NOT EXISTS `users_bookings_promo_code` (
  `booking_promo_code_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `is_use` tinyint(1) DEFAULT '0',
  `is_claim` tinyint(1) DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`booking_promo_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_bookings_rescheduled
CREATE TABLE IF NOT EXISTS `users_bookings_rescheduled` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_booking_id` int(11) DEFAULT NULL,
  `user_rescheduled_id` int(11) DEFAULT NULL,
  `user_rescheduled_name` varchar(255) DEFAULT NULL,
  `user_rescheduled_email` varchar(255) DEFAULT NULL,
  `user_recipient_id` int(11) DEFAULT NULL,
  `user_recipient_name` varchar(255) DEFAULT NULL,
  `user_recipient_email` varchar(255) DEFAULT NULL,
  `salon_name` varchar(255) DEFAULT NULL,
  `is_professional` tinyint(1) DEFAULT NULL,
  `pre_date` datetime DEFAULT NULL,
  `rescheduled_date` datetime DEFAULT NULL,
  `status` tinyint(3) DEFAULT NULL,
  `is_send` tinyint(1) NOT NULL DEFAULT '0',
  `noted` text,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_bookings_service
CREATE TABLE IF NOT EXISTS `users_bookings_service` (
  `user_booking_service_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_booking_id` int(11) DEFAULT NULL,
  `user_prof_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_manager_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `style_hair_id` int(11) DEFAULT NULL,
  `short_price` double DEFAULT NULL,
  `short_checked` tinyint(1) DEFAULT '0',
  `medium_price` double DEFAULT NULL,
  `medium_checked` tinyint(1) DEFAULT '0',
  `long_price` double DEFAULT NULL,
  `long_checked` tinyint(1) DEFAULT '0',
  `is_default` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_booking_service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_booking_review_code
CREATE TABLE IF NOT EXISTS `users_booking_review_code` (
  `booking_review_code_id` int(11) NOT NULL AUTO_INCREMENT,
  `review_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_booking_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_prof_id` int(11) DEFAULT NULL,
  `hair_salon_id` int(11) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_prof` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_prof` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salon_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_send` tinyint(1) NOT NULL DEFAULT '0',
  `is_review` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`booking_review_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_contact
CREATE TABLE IF NOT EXISTS `users_contact` (
  `contact_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_contact_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `mobile_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_like` tinyint(1) DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qb_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_email_change_booking_date
CREATE TABLE IF NOT EXISTS `users_email_change_booking_date` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_pro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_send` tinyint(1) DEFAULT '0',
  `fullname_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_pro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username_pro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salon_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_change` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `pre_date` date DEFAULT NULL,
  `new_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_friend
CREATE TABLE IF NOT EXISTS `users_friend` (
  `friend_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `invited` tinyint(1) DEFAULT '0',
  `is_count_promo_code` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_friend_promo_code
CREATE TABLE IF NOT EXISTS `users_friend_promo_code` (
  `friend_promo_code_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `is_use` tinyint(1) DEFAULT '0',
  `is_claim` tinyint(1) DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`friend_promo_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_haircare_brands
CREATE TABLE IF NOT EXISTS `users_haircare_brands` (
  `user_haircare_brand_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `haicare_brand_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_haircare_brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_history_view
CREATE TABLE IF NOT EXISTS `users_history_view` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `style_hair_id` int(11) DEFAULT NULL,
  `hair_salon_id` int(11) DEFAULT NULL,
  `user_prof_id` int(11) DEFAULT NULL,
  `count_view` int(11) NOT NULL DEFAULT '1',
  `last_view` datetime NOT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_invited_email
CREATE TABLE IF NOT EXISTS `users_invited_email` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_send` tinyint(1) DEFAULT '0',
  `is_accept` tinyint(1) DEFAULT '0',
  `invite_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_count_promo_code` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_job_title
CREATE TABLE IF NOT EXISTS `users_job_title` (
  `job_title_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`job_title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_like
CREATE TABLE IF NOT EXISTS `users_like` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `style_hair_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `is_like` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_login
CREATE TABLE IF NOT EXISTS `users_login` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token_device` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_manager_promotion
CREATE TABLE IF NOT EXISTS `users_manager_promotion` (
  `users_manager_promotion_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `promotion_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `term_of_condition` mediumtext COLLATE utf8_unicode_ci,
  `details` mediumtext COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `total_promotion` int(11) DEFAULT NULL,
  `promotion_start_date` datetime NOT NULL,
  `promotion_end_date` datetime NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_promotion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_publish` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`users_manager_promotion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_manger_service_promotion
CREATE TABLE IF NOT EXISTS `users_manger_service_promotion` (
  `manager_promotion_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_manager_promotion_id` int(11) DEFAULT NULL,
  `service_manager_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `short_price` float DEFAULT NULL,
  `short_price_promotion` double DEFAULT NULL,
  `short_promotion_checked` tinyint(1) DEFAULT '0',
  `medium_price` float DEFAULT NULL,
  `medium_price_promotion` double DEFAULT NULL,
  `medium_promotion_checked` tinyint(1) DEFAULT '0',
  `long_price` float DEFAULT NULL,
  `long_price_promotion` double DEFAULT NULL,
  `long_promotion_checked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`manager_promotion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_payment
CREATE TABLE IF NOT EXISTS `users_payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `user_stripe_id` varchar(250) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_reset_password
CREATE TABLE IF NOT EXISTS `users_reset_password` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_send` tinyint(1) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `insert_date` date DEFAULT NULL,
  `is_active` tinyint(3) DEFAULT '0',
  `is_send_second` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_review
CREATE TABLE IF NOT EXISTS `users_review` (
  `user_review_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_booking_id` int(11) NOT NULL,
  `style_hair_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_prof_id` int(11) NOT NULL,
  `hair_salon_id` int(11) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_prof` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_prof` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salon_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_of_rate` tinyint(1) DEFAULT NULL,
  `comment` mediumtext COLLATE utf8_unicode_ci,
  `date` datetime NOT NULL,
  `is_send` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_service_category
CREATE TABLE IF NOT EXISTS `users_service_category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_order` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `category_main_id` int(11) NOT NULL,
  `name_vi` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_service_manager
CREATE TABLE IF NOT EXISTS `users_service_manager` (
  `service_manager_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `short_price` double DEFAULT NULL,
  `short_checked` tinyint(1) DEFAULT '0',
  `medium_price` double DEFAULT NULL,
  `medium_checked` tinyint(1) DEFAULT '0',
  `long_price` double DEFAULT NULL,
  `long_checked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`service_manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_start
CREATE TABLE IF NOT EXISTS `users_start` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `style_hair_id` int(11) DEFAULT NULL,
  `start` smallint(2) DEFAULT NULL,
  `comment` mediumtext COLLATE utf8_unicode_ci,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.users_stripe
CREATE TABLE IF NOT EXISTS `users_stripe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `access_token` varchar(250) NOT NULL,
  `livemode` int(11) NOT NULL,
  `refresh_token` varchar(250) NOT NULL,
  `token_type` varchar(250) NOT NULL,
  `stripe_publishable_key` varchar(250) NOT NULL,
  `stripe_user_id` varchar(250) NOT NULL,
  `scope` varchar(250) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.user_booking_charge
CREATE TABLE IF NOT EXISTS `user_booking_charge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_booking_id` int(11) NOT NULL,
  `final_price` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `cancel_platform` int(11) NOT NULL,
  `cancel_prof` int(11) NOT NULL,
  `stripe_charge_id` varchar(250) NOT NULL,
  `cancel_platform_charge_id` varchar(250) NOT NULL,
  `cancel_prof_charge_id` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.user_notification
CREATE TABLE IF NOT EXISTS `user_notification` (
  `notification_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token_device` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_notification` tinyint(1) DEFAULT '0',
  `system_os` tinyint(1) DEFAULT '0',
  `type` tinyint(1) DEFAULT '0',
  `is_notification_booking` tinyint(1) DEFAULT '0',
  `is_notification_chat` tinyint(1) DEFAULT '0',
  `is_notification_review` tinyint(1) DEFAULT '0',
  `is_notification_comment` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.user_package_charge
CREATE TABLE IF NOT EXISTS `user_package_charge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `create_date` datetime NOT NULL,
  `stripe_charge_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `apple_charge_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `google_charge_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table myhabfit_api.user_view_logs
CREATE TABLE IF NOT EXISTS `user_view_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `style_hair_id` int(11) DEFAULT NULL,
  `hair_salon_id` int(11) DEFAULT NULL,
  `user_prof_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
